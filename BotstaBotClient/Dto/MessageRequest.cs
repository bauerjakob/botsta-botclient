﻿using System;
using Newtonsoft.Json;

namespace Botsta.BotClient.Dto
{
    public class MessagePart
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("button")]
        public MessageButton Button { get; set; }
    }

    public class MessageButton
    {
        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("postback")]
        public string Postback { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
